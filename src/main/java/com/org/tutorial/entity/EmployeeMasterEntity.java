package com.org.tutorial.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Employee_Master")
public class EmployeeMasterEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    private long mobile;
    private String city;

}
