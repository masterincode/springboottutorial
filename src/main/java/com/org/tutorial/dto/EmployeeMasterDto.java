package com.org.tutorial.dto;

import lombok.Data;

@Data
public class EmployeeMasterDto {

	private Long id;
	private String name;
	private String address;
	private long mobile;
	private String city;

}
