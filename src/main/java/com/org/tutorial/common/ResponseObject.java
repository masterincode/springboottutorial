package com.org.tutorial.common;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ResponseObject {

	private  Map<String,Object> data = null;
	
	public void addData(String key , Object value) {
		if(this.data == null) {
			data = new HashMap<>();
		}
		data.put(key, value);
	}

	public Map<String, Object> getData() {
		return data;
	}

}
