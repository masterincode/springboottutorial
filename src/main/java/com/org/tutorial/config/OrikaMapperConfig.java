package com.org.tutorial.config;

import com.org.tutorial.dto.EmployeeMasterDto;
import com.org.tutorial.entity.EmployeeMasterEntity;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class OrikaMapperConfig extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(EmployeeMasterDto.class, EmployeeMasterEntity.class)
                .byDefault()
                .register();
    }

    public <R, S , T> S covertDtoToEntity(Class<R> r , Class<S> s, T t) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(r,s);
        MapperFacade mapper = mapperFactory.getMapperFacade();
        return mapper.map(t,s);
    }

    public <R, S , T> S covertEntityToDto(Class<R> r , Class<S> s, T t) {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(r,s);
        MapperFacade mapper = mapperFactory.getMapperFacade();
        return mapper.map(t,s);
    }

}
