package com.org.tutorial.service;

import com.org.tutorial.config.OrikaMapperConfig;
import com.org.tutorial.dto.EmployeeMasterDto;
import com.org.tutorial.entity.EmployeeMasterEntity;
import com.org.tutorial.repo.EmployeeRepository;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private OrikaMapperConfig orikaMapper;
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();


    public Optional<EmployeeMasterEntity> getOneEmployee(Long employeeId) {
        return employeeRepository.findById(employeeId);
    }

    public Optional<EmployeeMasterEntity> getByRequestParam(Long employeeId) {
        return employeeRepository.findById(employeeId);
    }

    public Iterable<EmployeeMasterEntity> getAllEmployee() {
        return employeeRepository.findAll();
    }

    public EmployeeMasterDto saveEmployee(EmployeeMasterDto employeeMasterDto) {
        EmployeeMasterEntity entity =  orikaMapper.covertDtoToEntity(EmployeeMasterDto.class, EmployeeMasterEntity.class, employeeMasterDto);
        EmployeeMasterEntity fetchSaveDataEntity =  employeeRepository.save(entity);
        EmployeeMasterDto fetchSaveDataDto = orikaMapper.covertEntityToDto(EmployeeMasterEntity.class, EmployeeMasterDto.class, fetchSaveDataEntity);
        return fetchSaveDataDto;
    }
}
