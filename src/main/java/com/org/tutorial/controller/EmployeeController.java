package com.org.tutorial.controller;

import com.org.tutorial.dto.EmployeeMasterDto;
import com.org.tutorial.entity.EmployeeMasterEntity;
import com.org.tutorial.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	//http://localhost:8080/tutorial/getOneEmployee/102
	@PostMapping("getOneEmployee/{employeeId}")
	public Optional<EmployeeMasterEntity> getOneEmployee(@PathVariable Long employeeId) {
		return employeeService.getOneEmployee(employeeId);
	}

	//http://localhost:8080/tutorial/getByRequestParam?employeeId=101
	@PostMapping("getByRequestParam")
	public Optional<EmployeeMasterEntity> getByRequestParam(@RequestParam(value="employeeId") long employeeId) {
		return employeeService.getByRequestParam(employeeId);
	}

	//http://localhost:8080/tutorial/getAllEmployee/
	@GetMapping("getAllEmployee")
	public Iterable<EmployeeMasterEntity> getAllEmployee() {
		return employeeService.getAllEmployee();
	}

	//http://localhost:8080/tutorial/saveEmployee
	@PostMapping("saveEmployee")
	public EmployeeMasterDto saveEmployee(@RequestBody EmployeeMasterDto employeeMasterDto) {
		return employeeService.saveEmployee(employeeMasterDto);
	}

}
