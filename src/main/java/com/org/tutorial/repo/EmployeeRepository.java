package com.org.tutorial.repo;

import com.org.tutorial.entity.EmployeeMasterEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeMasterEntity,Long> {

}
